#!/usr/bin/env python3

import requests
import argparse
import os
import re
from bs4 import BeautifulSoup


def markdown_filter(text):
    return re.sub(r"(---(\s+Day\s+\d+\:\s+\w+.*\s+)---).*", r"#\2", text)


def fetch_aoc_challenge(year, day, url_base='https://adventofcode.com'):
    """
    Fetches AOC challenge text by year, day and type
    """

    aoc_page = None

    aoc_url = '{url_base}/{year}/day/{day}'.format(url_base=url_base,
                                                   year=year,
                                                   day=day)

    try:
        aoc_req = requests.get(aoc_url)

        if aoc_req.status_code == 200:
            aoc_page = BeautifulSoup(aoc_req.text, 'html.parser').find(class_='day-desc').text

    except Exception:

        aoc_page = None

    finally:

        return aoc_page


def main():

    parser = argparse.ArgumentParser(
        description = "Fetches Advent of Code challenge and half-ass README-ifies it"
    )

    parser.add_argument(
        '--year',
        dest='year',
        type=int,
        required=True
    )

    parser.add_argument(
        '--day',
        dest='day',
        type=int,
        required=True
    )

    args = parser.parse_args()


    print("Fetching challenge for {year}/{day}... ".format(year=args.year, day=args.day), end="")

    challenge_text = fetch_aoc_challenge(args.year, args.day)

    if challenge_text:

        print("hurray!")

        challenge_dir = 'day%02d' % args.day
        challenge_file = os.path.join(challenge_dir, 'README.md')

        print("Stashing challenge in {file}... ".format(file=challenge_file), end="")

        try:
            if not os.path.isdir(challenge_dir):
                os.mkdir(challenge_dir)


            if not os.path.isfile(challenge_file):

                with open(challenge_file, "w") as fh:
                    fh.write(markdown_filter(challenge_text))

            print("hurray!")

        except Exception as error:
            print("bah humbug")
            print()
            print('Error: %s' % error)

            sys.exit(1)

    else:
        print("bah humbug")
        print()
        print('Error: Unable to fetch the challenge for {year}/{day}.  Bah humbug, indeed.'.format(year=args.year,
                                                                                                     day=args.day))
        sys.exit(2)


if __name__ == '__main__':
    main()
