#!/usr/bin/env python3
from itertools import combinations
import argparse
import math


def mul(pair):
    """
    Multiply values in a tuple, list, whatev
    """

    return math.prod(pair)


def sum2020Multiply(pair, magic_sum=2020, debug=False):
    """
    Day01 algorithm 

    If two values (val1 and val2)
    sum to 2020, then return their 
    multipled together value

    """

    result = sum(pair)
    
    if debug:
        print('{pair} == {result}'.format(pair=pair, result=result), flush=True)

    if result == magic_sum:
        return (mul(pair), pair)


def main():
    
    parser = argparse.ArgumentParser(description='AOC Day 01 solution')

    parser.add_argument(
        '--input-file',
        '-f',
        dest='input_file',
        action='store',
        default='input.txt'
    )

    parser.add_argument(
        '--combinations',
        '-c',
        dest='combinations',
        action='store',
        type=int,
        default=2
    )

    parser.add_argument(
        '--debug',
        '-d',
        dest='debug',
        action='store_true',
        default=False
    )

    # Parse the args
    args = parser.parse_args()

    # Open + load inputs
    with open(args.input_file, 'r') as fh:
        inputs = [ int(item.strip()) for item in fh.readlines() ]

    if args.debug:
        print("Input length: {length}".format(length=len(inputs)))

    # Generate combinations
    c = combinations(inputs, args.combinations)

    # Filter out non-successful results (e.g. None)
    result = filter(None, map(sum2020Multiply, list(c)))

    # Print the result
    print(list(result))


if __name__ == '__main__':
    main()
