#!/usr/bin/env python3
import argparse
import re


class tobogganPolicyException(Exception):
    pass


class tobogganPassword:

    def __init__(self, raw_policy, password, part):

        self._prange, self.pchar = raw_policy.split()  # TODO: prob some whitespace sanitize I could do
        
        self.min_prange, self.max_prange = self._generatePolicyRange(self._prange)

        self.password = password

        self.char_count = None

        self.part = part

        self._valid = self._validatePassword(self.part)

    def _generatePolicyRange(self, prange):

        try:
            return sorted([int(n) for n in prange.split("-")], reverse=False)

        except Exception as error:

            raise tobogganPolicyException("Unable to generate policy: %s" % error)

    def _validatePassword(self, part):

        if part == 1:
            # Range / occurance pbased olicy
            self.char_count = len(re.findall(r"{pchar}".format(pchar=self.pchar), self.password))

            if self.char_count >= self.min_prange and self.char_count <= self.max_prange:

                return True
            else:

                return False

        elif part == 2:
            # Index only one exact match 
            found = False

            try:
                if self.password[self.min_prange - 1] == self.pchar:

                    found = True
                if self.password[self.max_prange - 1] == self.pchar:

                    # If we already found a match, then if we match
                    # again, we break the policy.  Reset to False (not found)
                    if found:
                        found = False
                    else:
                        found = True

            except IndexError:

                found = False
            finally:

                return found

        else:
            raise NotImplementedError('The part you are seeking is partless!')

    def setPolicy(self, policy):

        raise NotImplementedError()

    def isValid(self):

        return self._valid

    def checkPolicy(self):

        self._validatePassword(self.part)


def main():
    
    parser = argparse.ArgumentParser(description='AOC Day 02 solution')

    parser.add_argument(
        '--input-file',
        '-f',
        dest='input_file',
        action='store',
        default='input.txt'
    )

    parser.add_argument(
        '--part',
        '-p',
        dest='part',
        action='store',
        choices=(1, 2),
        type=int,
        default=1
    )

    parser.add_argument(
        '--debug',
        '-d',
        dest='debug',
        action='store_true',
        default=False
    )

    # Parse the args
    args = parser.parse_args()

    # Open + load inputs
    with open(args.input_file, 'r') as fh:
        inputs = [ item.strip() for item in fh.readlines() ]

    if args.debug:
        print("Input length: {length}".format(length=len(inputs)))


    valid_password_count = 0

    # Start checkin' passwords!
    for line in inputs:
        raw_policy, password = line.split(":")

        tp = tobogganPassword(raw_policy.strip(), password.strip(), part=args.part)

        if tp.isValid():
            valid_password_count += 1


    print()
    print("Valid password(s): {valid} out of {total}".format(valid=valid_password_count, total=len(inputs)))


if __name__ == '__main__':
    main()
