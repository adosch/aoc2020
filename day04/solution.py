#!/usr/bin/env python3
import argparse
import re
import os
import pprint


class InputProcessor:

    def __init__(self, inputFile='input.txt'):

        self.inputFile = self._is_file(inputFile)

        self._inputData = self._loadInput(self.inputFile)

        self._newline = "\n"

        # BAH HUMBUG!  Noticed hcl has a pound(#) sign that doesnt
        # match with \w class
        self._data_regex = r"[a-z]{3}\:\#?\w+"

    def _loadInput(self, f, mode='r'):

        try:
            with open(f, mode) as fh:
                data = fh.readlines()

            return data

        except Exception as error:

            raise IOError("Unable to read file: %s" % error)

    def _is_file(self, f):

        if os.path.isfile(f):
            return f
        else:
            raise FileNotFoundError('%s is not a file or does not exist' % f)

    def _matchData(self, line):

        if re.search(self._data_regex, line):
            return True

        return False

    def getRecord(self):

        buffer = []

        for line in self._inputData:

            if self._matchData(line):
                buffer.append(line.strip())

            if line == self._newline:
                record = buffer
                buffer = []

                yield(record)

        yield(buffer)


class Passport:

    def __init__(self, record):

        self._passport_keys = (
            'byr',
            'iyr',
            'eyr',
            'hgt',
            'hcl',
            'ecl',
            'pid',
            'cid'
        )

        # Part 2 addition hack-in functionality (nothing for 'cid')
        self._passport_regex = {
            'byr': r'19[2-9][0-9]|200[0-2]',
            'iyr': r'201[0-9]|2020',
            'eyr': r'202[0-9]|2030',
            'hgt': r'(?:15[0-9]|1[6-8][0-9]|19[0-3])cm|(?:59|6[0-9]|7[0-6])in',
            'hcl': r'#[0-9a-f]{6}',
            'ecl': r'amb|blu|brn|gry|grn|hzl|oth',
            'pid': r'[0-9]{9}',
            'cid': r'.*'
        }

        # BAH HUMBUG!  Noticed hcl has a pound(#) sign that doesnt
        # match with \w class
        self._data_regex = r"[a-z]{3}\:\#?\w+"

        self._raw_record = record

        self.__initialize_attrs()
        self.__load_raw()

        self.validity = False


    def __initialize_attrs(self):

        for attr in self._passport_keys:
            self.__setattr__(attr, None)

    def __load_raw(self):

        pfield = re.compile(self._data_regex)
        matches = pfield.findall(" ".join(self._raw_record))  # mando separatation beween list elems

        # hacktastic
        for match in matches:
            k, v = match.split(":")

            if k in self.__dict__:
                self.__setattr__(k, v)

    def _validate_passport_field(self, key):

        if (key in self.__dict__) and self.__getattribute__(key) != None:
            return (key, True)

        return (key, False)

    def _validate_passport_indepth(self, key):

        validation = False

        try:
            if (key in self.__dict__) and self.__getattribute__(key) != None:

                # Pass reg-ex check
                if re.fullmatch(self._passport_regex[key], self.__getattribute__(key)):

                    validation = True

                else:
                    validation = False
            else:
                validation = False

        except KeyError as error:
            print("Validation failure: %s" % error)
            validation = False

        finally:
            return (key, validation)


    def isValid(self, skip_cid=True, part=1):

        if part == 1:
            results = {k: v for k, v in map(self._validate_passport_field, self._passport_keys)}

        elif part == 2:
            results = {k: v for k, v in map(self._validate_passport_indepth, self._passport_keys)}

        else:
            raise NotImplementedError("Part %s it not implemented, stupid hermes." % part)

        if skip_cid:
            results.pop('cid')

        print(results)

        if False in results.values():
            self.validity = False

            return self.validity

        self.validity = True

        return self.validity


def main():

    parser = argparse.ArgumentParser(description='AOC Day 04 solution')

    parser.add_argument(
        '--input-file',
        '-f',
        dest='input_file',
        action='store',
        default='input.txt'
    )

    parser.add_argument(
        '--part',
        '-p',
        dest='part',
        action='store',
        choices=(1, 2),
        type=int,
        default=1
    )

    parser.add_argument(
        '--debug',
        '-d',
        dest='debug',
        action='store_true',
        default=False
    )

    # Parse the args
    args = parser.parse_args()

    ip = InputProcessor(inputFile=args.input_file)

    total_pp = 0
    valid_pp = 0

    for record in ip.getRecord():
        total_pp = total_pp + 1

        p = Passport(record)
        pprint.pprint(p.__dict__)

        if p.isValid(part=args.part):
            valid_pp = valid_pp + 1

        print("Valid: {valid}".format(valid=p.validity))
        print("--------")

    print("Total: {total}".format(total=total_pp))
    print("Valid: {valid}".format(valid=valid_pp))


if __name__ == '__main__':
    main()
