#!/usr/bin/env python3
import argparse


def main():
    
    parser = argparse.ArgumentParser(description='AOC Day 03 solution')

    parser.add_argument(
        '--input-file',
        '-f',
        dest='input_file',
        action='store',
        default='input.txt'
    )

    parser.add_argument(
        '--part',
        '-p',
        dest='part',
        action='store',
        choices=(1, 2),
        type=int,
        default=1
    )

    parser.add_argument(
        '--debug',
        '-d',
        dest='debug',
        action='store_true',
        default=False
    )

    # Parse the args
    args = parser.parse_args()


    # TODO:
    #
    # Not sure this should be multi-dimensional, the 
    # right 3, down 1 pattern in combation with the 
    # character count of each map line would never
    # make it ever to the bottom without a full
    # restart to the "new" start position.  
    #
    # It's either static char-length + 3 read to the
    # end and combine map output serially?
    #


    map = []

    # Open + load inputs
    fh = open(args.input_file, 'r')

    for line in fh:

        map.append(list(line.strip()))

    fh.close()



    if args.debug:
        print("Input length: {length}".format(length=len(inputs)))


if __name__ == '__main__':
    main()
